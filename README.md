A News app using React and Next.js

## Features
- PWA for offline access.
- Infinite scroll pagination.
- Responsive design.



## Local Development
This project was bootstrapped with [Next.js](https://nextjs.org/).
Below is a list of commands you will probably find useful.

### Setup
Install dependencies:
```bash
yarn
```


### `yarn dev`
Runs the project in development. Fast Refresh will update the code only for that file, and re-render your component.


## Build for production
### `yarn dev`
Creates an optimized production build of application. The output displays information about each route.


## Start built version
### `yarn dev`
starts the application in production mode.


## Start developing UI in isolation
### `yarn storybook`


## Run test
### `yarn test`
Runs the test using (Jest)


## Environment variables
| Environment | Required? | Example Value | Description |
|:-- |:--:|:-- |:-- |
|`NEXT_PUBLIC_API_URL`| ✓ | https://content.guardianapis.com | - |
|`NEXT_PUBLIC_API_KEY`| ✓ | test | - |
