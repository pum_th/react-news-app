export type MediaSizeType = {
  xs: string
  sm: string
  md: string
  lg: string
  xl: string
}

export type MediaQueryType = {
  media: MediaSizeType
}

export type BreakpointType = {
  breakpoints: MediaSizeType
}

export type ThemeType = MediaQueryType & BreakpointType
