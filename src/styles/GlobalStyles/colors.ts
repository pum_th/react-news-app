import { css } from '@emotion/core'

export default css`
  :root {
    --color-primary-light: #134390;
    --color-primary: #0f3674;
    --color-primary-dark: #0e326c;

    --color-gray-light: #f7f7f7;
    --color-gray: #f0f0f0;
    --color-gray-dark: #b3b3b3;

    --color-negative-bg: #fdf0ef;
    --color-negative-border: #f4afa7;
    --color-negative: #e54937;

    --color-news: #388e3c;
    --color-sport: #f50057;
    --color-culture: #ffca28;
    --color-lifeandstyle: #2196f3;
  }
`
