export const sortByDate = (bookmarks, sortBy) => {
  const sorted = bookmarks.sort((a, b) => {
    if (sortBy === 'newest') {
      return a.date < b.date ? 1 : a.date > b.date ? -1 : 0
    }

    return a.date < b.date ? -1 : a.date > b.date ? 1 : 0
  })

  return sorted
}
