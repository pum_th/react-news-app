import { sortByDate } from './helpers'

describe('Bookmark Helpers', () => {
  describe('Sort by date', () => {
    it('should sorting by newest', () => {
      const mockList = [
        {
          id: 1,
          date: '2020-08-09T01:20:41Z',
        },
        {
          id: 2,
          date: '2020-06-09T01:20:41Z',
        },
        {
          id: 3,
          date: '2020-10-09T01:20:41Z',
        },
      ]

      const sorted = sortByDate(mockList, 'newest')

      expect(sorted[0].id).toBe(3)
      expect(sorted[1].id).toBe(1)
      expect(sorted[2].id).toBe(2)
    })

    it('should sorting by oldest', () => {
      const mockList = [
        {
          id: 1,
          date: '2020-08-09T01:20:41Z',
        },
        {
          id: 2,
          date: '2020-06-09T01:20:41Z',
        },
        {
          id: 3,
          date: '2010-10-09T01:20:41Z',
        },
      ]

      const sorted = sortByDate(mockList, 'oldest')

      expect(sorted[0].id).toBe(3)
      expect(sorted[1].id).toBe(2)
      expect(sorted[2].id).toBe(1)
    })
  })
})
