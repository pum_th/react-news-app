export type SingleContentType = {
  id: string
  title: string
  thumbnail: string
  section: string
  headline: string
  rawDate: string
  publicationDate: string
  body: string
}
