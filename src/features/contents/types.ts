export type Post = {
  id: string
  title: string
  thumbnail: string
  section: string
  body: string
}

export type ContentType = {
  currentPage: string
  pages: string
  results: Post[]
}

export interface ListProps {
  posts: Post[]
}
