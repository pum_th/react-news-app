import React from 'react'

import CircularProgress from './CircularProgress'

export const Overview = () => {
  return (
    <div>
      <CircularProgress />
    </div>
  )
}

export default {
  title: 'CircularProgress',
}
