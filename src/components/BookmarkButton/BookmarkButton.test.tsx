import React from 'react'

import { render, screen } from '@/tests/test-utils'
import BookmarkButton from './BookmarkButton'

describe('BookmarkButton', () => {
  it('should render correctly', () => {
    render(<BookmarkButton />)

    expect(screen.getByText('View Bookmark')).toBeInTheDocument()
  })
  it('should render custom text correctly', () => {
    render(<BookmarkButton text="Add to bookmark" />)

    expect(screen.getByText('Add to bookmark')).toBeInTheDocument()
  })
})
