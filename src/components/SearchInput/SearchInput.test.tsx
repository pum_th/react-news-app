import React from 'react'

import { render, screen, fireEvent } from '@/tests/test-utils'

import SearchInput from './SearchInput'

describe('SearchInput', () => {
  it('should render correctly', () => {
    render(<SearchInput onChange={() => {}} />)

    expect(screen.queryByPlaceholderText('Search all news')).toBeNull()

    fireEvent.click(screen.getByTestId('searchButton'))

    expect(screen.queryByPlaceholderText('Search all news')).toBeInTheDocument()
  })
})
