import React, { useState } from 'react'

import { SeoTags } from '@/lib/Seo'
import { SelectInput } from '@/components/SelectInput'
import { PageHeader } from '@/components/Layouts/PageHeader'

import { useLocalBookmarks, sortByDate } from '@/features/bookmarks'
import { List } from '@/features/contents'

function Bookmarks() {
  const [orderBy, setOrderBy] = useState('newest')
  const localBookmarks = useLocalBookmarks()
  const sectionTitle = 'All bookmark'

  const bookmarks = sortByDate(localBookmarks.bookmarks, orderBy)

  return (
    <div>
      <SeoTags title={sectionTitle} />
      <PageHeader
        title={sectionTitle}
        renderSorting={() => (
          <SelectInput
            initialValue={orderBy}
            onChange={(value) => {
              setOrderBy(value)
            }}
            options={[
              {
                label: 'Newest First',
                value: 'newest',
              },
              {
                label: 'Oldest First',
                value: 'oldest',
              },
            ]}
          />
        )}
      />
      <div>
        <List posts={bookmarks} />
      </div>
    </div>
  )
}

export default Bookmarks
