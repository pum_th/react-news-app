import React from 'react'
import { useRouter } from 'next/router'

import { LoadMore, useInfiniteQueryContents } from '@/features/contents'
import { List } from '@/features/contents'
import { SeoTags } from '@/lib/Seo'
import { SelectInput } from '@/components/SelectInput'
import { Loading } from '@/components/Loading'
import { ContentType } from '@/features/contents/types'
import { PageHeader } from '@/components/Layouts/PageHeader'

function Search() {
  const { query } = useRouter()
  const [orderBy, setOrderBy] = React.useState('newest')

  const queryResult = useInfiniteQueryContents('', {
    orderBy,
    q: query.q as string,
  })

  const { canFetchMore, status, fetchMore, isFetchingMore } = queryResult
  const data = queryResult.data as ContentType[]

  return (
    <div>
      <SeoTags title={`Search: ${query.q || ' Not Found'}`} />
      <PageHeader
        title="Search result"
        renderSorting={() => (
          <SelectInput
            initialValue={orderBy}
            onChange={(value) => {
              setOrderBy(value)
            }}
            options={[
              {
                label: 'Newest First',
                value: 'newest',
              },
              {
                label: 'Oldest First',
                value: 'oldest',
              },
            ]}
          />
        )}
      />
      <div>
        {status !== 'success' ? (
          <Loading />
        ) : (
          <div>
            {data.map((group) => {
              return <List key={group.currentPage} posts={group.results} />
            })}
          </div>
        )}
      </div>
      {canFetchMore && (
        <div css={{ display: 'flex', justifyContent: 'center' }}>
          <LoadMore isLoading={!!isFetchingMore} onClick={() => fetchMore()}>
            {!!isFetchingMore ? 'Loading' : 'Load more'}
          </LoadMore>
        </div>
      )}
    </div>
  )
}

export default Search
