module.exports = {
  stories: ['../src/**/*.stories.@(ts|tsx|js|jsx|mdx)'],
  addons: [{
    name: '@storybook/addon-essentials',
  }]
}
